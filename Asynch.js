'use strict';
function f1(){
    console.log("f_1");
}
function f2(){
    console.log("f_2");
}
function f3(){
    console.log("f_3");
}
function f4(){
    console.log("f_4");
}
function f5(){
    console.log("f_5");
}
function greeting(am_pm){
    if(am_pm === 'am'){
        console.log("good morning");
    } else {
        console.log("good evening");
    }
}
function test1(){
    greeting('am');
    f1();
    f2();
    f3();
    f4();
    greeting('pm');
}
function test2(){
    setTimeout(greeting, 2000, 'pm');
}
function test3(){
    let timer = setInterval(greeting, 2000, 'pm');
    timer;
    setTimeout(clearInterval, 6000, timer);
}